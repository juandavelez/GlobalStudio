﻿namespace GlobalStudio.Controllers
{
using System.Data.Entity;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using GlobalStudio.Context;
using GlobalStudio.Models;
using GlobalStudio.Services;
using Newtonsoft.Json;
    using System.Collections.Generic;

    public class CompaniesController : Controller
    {
        private ApiService apiService;
        private Parameters parameter;
        private GetSalesData getSalesData;
        private CompanyContext db = new CompanyContext();
        List<Company> companyList = new List<Company>();

        // GET: Companies
        public async Task<ActionResult> Index()
        {
            /*apiService = new ApiService();
            parameter = new Parameters();
            getSalesData = new GetSalesData();


            getSalesData.CompanyCode = "GRA";
            getSalesData.SessionId = "64CD19D0-89CD-4F9C-8101-5118C5F0106D";

            parameter.parameters = getSalesData;

            var request = JsonConvert.SerializeObject(parameter);
            var urlAPI = "http://gsdataservice.azurewebsites.net/Service.svc/";

            var response = await apiService.Post(
                urlAPI,
                "GetSalesData",
                parameter);
            var companies = response.Result;*/
  
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Mapping()
        {
            apiService = new ApiService();
            parameter = new Parameters();
            getSalesData = new GetSalesData();

            getSalesData.CompanyCode = "GRA";
            getSalesData.SessionId = "64CD19D0-89CD-4F9C-8101-5118C5F0106D";

            parameter.parameters = getSalesData;

            var request = JsonConvert.SerializeObject(parameter);
            var urlAPI = "http://gsdataservice.azurewebsites.net/Service.svc/";

            var response = await apiService.PostJson(
                urlAPI,
                "GetSalesData",
                parameter);
            var companyL = response;

            return companyL;
        }


        [HttpPost]
        public bool Guardar( Company company )
        {
            companyList.Add(company);
            return true;
        }

        [HttpPost]
        public async Task<ActionResult> ObtenerBD()
        {
            return View(companyList);
        }











        /*// GET: Companies/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = await db.Company.FindAsync(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            return View(company);
        }

        // GET: Companies/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Companies/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "CompanyCode,OrderNumber,InvoiceNumber,DateSold,CustomerCode,CustomerName,CustomerAddress,ItemReference,Quantity,CostPerUnit")] Company company)
        {
            if (ModelState.IsValid)
            {
                db.Company.Add(company);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(company);
        }

        // GET: Companies/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = await db.Company.FindAsync(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            return View(company);
        }

        // POST: Companies/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "CompanyCode,OrderNumber,InvoiceNumber,DateSold,CustomerCode,CustomerName,CustomerAddress,ItemReference,Quantity,CostPerUnit")] Company company)
        {
            if (ModelState.IsValid)
            {
                db.Entry(company).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(company);
        }

        // GET: Companies/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = await db.Company.FindAsync(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            return View(company);
        }

        // POST: Companies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            Company company = await db.Company.FindAsync(id);
            db.Company.Remove(company);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }*/
    }
}
