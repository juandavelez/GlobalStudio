﻿using GlobalStudio.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GlobalStudio.Services
{
    public class FormatToJson
    {

        public List<Company> StringToCompany (string json){
            try
            {
                var Inicio = json.IndexOf("[");
                var Fin = json.IndexOf("]") - (Inicio-1);
                var result = json.Substring(Inicio, Fin);

                var list = JsonConvert.DeserializeObject<List<Company>>(result);

                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string StringToCompanyJson(string json)
        {
            try
            {
                var Inicio = json.IndexOf("[");
                var Fin = json.IndexOf("]") - (Inicio - 1);
                var result = json.Substring(Inicio, Fin);

                //var list = JsonConvert.DeserializeObject<List<Company>>(result);

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}