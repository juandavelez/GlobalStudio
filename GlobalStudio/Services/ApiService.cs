﻿namespace GlobalStudio.Services
{
    using GlobalStudio.Models;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web.Http.Results;
    using System.Web.Mvc;
    using System.Web.Script.Serialization;

    public class ApiService
    {


        public async Task<Response> Post<T>(
            string urlBase, string servicePrefix, T model)
        {
            try
            {
                //var request = JsonConvert.SerializeObject(model);
                var request = JsonConvert.SerializeObject(model);
                var content = new StringContent(request, Encoding.UTF8, "application/json");
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);
                var url = string.Format("{0}", servicePrefix);
                var response = await client.PostAsync(url, content);

                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = response.StatusCode.ToString(),
                    };
                }

                var result = (await response.Content.ReadAsStringAsync()).Replace("\\", "");

                FormatToJson formatToJson = new FormatToJson();
                var resultado = formatToJson.StringToCompany(result);

                return new Response
                {
                    IsSuccess = true,
                    Message = "Record added OK",
                    Result = resultado,
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message,
                };
            }
        }

        public async Task<JsonResult> PostJson<T>(
         string urlBase, string servicePrefix, T model)
        {
            try
            {
                //var request = JsonConvert.SerializeObject(model);
                var request = JsonConvert.SerializeObject(model);
                var content = new StringContent(request, Encoding.UTF8, "application/json");
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);
                var url = string.Format("{0}", servicePrefix);
                var response = await client.PostAsync(url, content);


                var result = (await response.Content.ReadAsStringAsync()).Replace("\\", "");

                FormatToJson formatToJson = new FormatToJson();
                var resultado = formatToJson.StringToCompanyJson(result);
                var resultado2 = new JsonResult
                {
                    Data = new { resultado }
                };

                return resultado2;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }

}