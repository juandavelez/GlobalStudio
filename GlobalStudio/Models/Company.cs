﻿namespace GlobalStudio.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Company
    {
        [Key]
        public string CompanyCode { get; set; }

        //[Required(ErrorMessage = "The field {0} is required.")]
        //[MaxLength(50, ErrorMessage = "The field {0} , only can contain {1} characters lenght.")]
        //[Index("OrderNumber_Index", IsUnique = true)]
        public string OrderNumber { get; set; }

        //[Required(ErrorMessage = "The field {0} is required.")]
        //[MaxLength(50, ErrorMessage = "The field {0} , only can contain {1} characters lenght.")]
        //[Index("InvoiceNumber_Index", IsUnique = true)]
        public string InvoiceNumber { get; set; }

        //[Required(ErrorMessage = "The field {0} is required.")]
        //[MaxLength(50, ErrorMessage = "The field {0} , only can contain {1} characters lenght.")]
        //[Index("DateSold_Index", IsUnique = true)]
        public string DateSold { get; set; }

        //[Required(ErrorMessage = "The field {0} is required.")]
        //[MaxLength(50, ErrorMessage = "The field {0} , only can contain {1} characters lenght.")]
        //[Index("CustomerCode_Index", IsUnique = true)]
        public string CustomerCode { get; set; }

        //[Required(ErrorMessage = "The field {0} is required.")]
        //[MaxLength(50, ErrorMessage = "The field {0} , only can contain {1} characters lenght.")]
        //[Index("CustomerName_Index", IsUnique = true)]
        public string CustomerName { get; set; }

        // [Required(ErrorMessage = "The field {0} is required.")]
        //[MaxLength(50, ErrorMessage = "The field {0} , only can contain {1} characters lenght.")]
        //[Index("CustomerAddress_Index", IsUnique = true)]
        public string CustomerAddress { get; set; }

        //[Required(ErrorMessage = "The field {0} is required.")]
        // [MaxLength(50, ErrorMessage = "The field {0} , only can contain {1} characters lenght.")]
        // [Index("ItemReference_Index", IsUnique = true)]
        public string ItemReference { get; set; }

        //[Required(ErrorMessage = "The field {0} is required.")]
        //[MaxLength(50, ErrorMessage = "The field {0} , only can contain {1} characters lenght.")]
        //[Index("Quantity_Index", IsUnique = true)]
        public int Quantity { get; set; }

        //  [Required(ErrorMessage = "The field {0} is required.")]
        //[MaxLength(50, ErrorMessage = "The field {0} , only can contain {1} characters lenght.")]
        //[Index("CostPerUnit_Index", IsUnique = true)]
        public double CostPerUnit { get; set; }
    }
}