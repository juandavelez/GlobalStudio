﻿namespace GlobalStudio.Models
{
    using System;

    public class GetSalesData
    {
        public String SessionId { get; set; }

        public String CompanyCode { get; set; }
    }
}