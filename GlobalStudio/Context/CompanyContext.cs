﻿namespace GlobalStudio.Context
{
    using GlobalStudio.Models;
    using System.Data.Entity;

    public class CompanyContext : DbContext
    {
        public DbSet<Company> Company { get; set; }
    }
}